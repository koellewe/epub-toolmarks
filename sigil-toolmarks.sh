#!/bin/bash

export TOOL="Sigil"

. base.sh

OPF_PATH="OEBPS/content.opf"
OPFS=$(find . -name "*.opf" | wc -l)
if [[ $OPFS -eq 1 ]]; then
  if [ ! -f "$OPF_PATH" ]; then
    fail "File '$OPF_PATH' missing."
  fi
else
  fail "Bad number of OPF files. Expected 1 but found ${OPFS}."
fi

EPUB_VERSION=$(grep -m 1 "<package" "$OPF_PATH" | sed -e 's/.*version\="//' -e 's/".*//')
if [[ "$EPUB_VERSION" != "2.0" ]]; then
  fail "Wrong EPUB version. Expected 2.0 but found $EPUB_VERSION."
fi

CHAPTER1_PATH='OEBPS/Text/Section0001.xhtml'
if [[ ! -f $CHAPTER1_PATH ]]; then
  fail "First chapter file '$CHAPTER1_PATH' missing."
fi

TWO_SPACES=0
IFS=''  # ensures spaces are included in lines read
while read line; do
  indent_spaces=$(echo -n "${line//<*/}" | sed -e 's/\(.\)/\1\n/g' | { grep -c ' ' || true; })
  if [[ ! ($indent_spaces -eq 0 || $indent_spaces -eq 2 || $indent_spaces -eq 4) ]]; then
    fail "Bad indentation in OPF file. Expected a multiple of 2 spaces (max 4), but received $indent_spaces."
  fi
  if [[ $indent_spaces -eq 2 ]]; then
    TWO_SPACES=1
  fi
done < $OPF_PATH

if [[ TWO_SPACES -ne 1 ]]; then
  fail "Bad indentation in OPF file. Not formatted with two spaces."
fi

CONTAINER_PATH="META-INF/container.xml"
while read line; do
  indent_spaces=$(echo -n "${line//<*/}" | sed -e 's/\(.\)/\1\n/g' | { grep -c ' ' || true; })
  if [[ $indent_spaces -eq 2 ]]; then
    fail "Bad indentation in container.xml. Found 2-space indentation, but expected 4."
  fi
done < $CONTAINER_PATH

stylesheets=$(find . -name '*.css' | wc -l)
if [[ $stylesheets -gt 0 ]]; then
  fail "Unexpectedly found stylesheets."
fi

TOC_PATH='OEBPS/toc.ncx'
if [[ ! -f $TOC_PATH ]]; then
  fail "Table of contents file '$TOC_PATH' missing."
fi

TOC_NAV_LINE5=$(grep -m 1 -A 5 "<navPoint" $TOC_PATH | tail -n 1 | tr -d '[:blank:]' | sed -e 's/\r$//')
if [[ $TOC_NAV_LINE5 != "</navPoint>"* ]]; then
  fail "Expected TOC <navPoint> tag to span exactly 6 lines."
fi

# Finally, print out
success
