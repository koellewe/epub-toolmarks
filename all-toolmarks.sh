#!/bin/bash

if [[ $# -lt 1 ]]; then
  echo "Please specify the file to be tested."
  exit 1
fi

./epubee-toolmarks.sh "$1"
./sigil-toolmarks.sh "$1"
./pressbooks_ev2-toolmarks.sh "$1"
./pressbooks_ev3-toolmarks.sh "$1"
