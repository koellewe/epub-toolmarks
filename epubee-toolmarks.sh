#!/bin/bash

export TOOL="EpubeeMaker"

. base.sh

OPF_PATH="OEBPS/content.opf"
OPFS=$(find . -name "*.opf" | wc -l)
if [[ $OPFS -eq 1 ]]; then
  if [ ! -f "$OPF_PATH" ]; then
    fail "File '$OPF_PATH' missing."
  fi
else
  fail "Bad number of OPF files. Expected 1 but found ${OPFS}."
fi

EPUB_VERSION=$(grep -m 1 "<package" "$OPF_PATH" | sed -e 's/.*version\="//' -e 's/".*//')
if [[ "$EPUB_VERSION" != "2.0" ]]; then
  fail "Wrong EPUB version. Expected 2.0 but found $EPUB_VERSION."
fi

CHAPTER_CONTENT_PATH='OEBPS/Text/content.xhtml'
if [[ ! -f $CHAPTER_CONTENT_PATH ]]; then
  fail "Chapter content file '$CHAPTER_CONTENT_PATH' missing."
fi

TWO_SPACES=0
IFS=''  # ensures spaces are included in lines read
while read line; do
  indent_spaces=$(echo -n "${line//<*/}" | sed -e 's/\(.\)/\1\n/g' | { grep -c ' ' || true; })
  if [[ ! ($indent_spaces -eq 0 || $indent_spaces -eq 2 || $indent_spaces -eq 4) ]]; then
    fail "Bad indentation. Expected a multiple of 2 spaces (max 4), but received $indent_spaces."
  fi
  if [[ $indent_spaces -eq 2 ]]; then
    TWO_SPACES=1
  fi
done < $OPF_PATH

if [[ TWO_SPACES -ne 1 ]]; then
  fail "Bad indentation. Not formatted with two spaces."
fi

STYLESHEET_PATH='OEBPS/Styles/stylesheet.css'
stylesheets=$(find . -name '*.css' | wc -l)
if [[ $stylesheets -eq 1 ]]; then
  if [[ ! -f $STYLESHEET_PATH ]]; then
    fail "Stylesheet file '$STYLESHEET_PATH' missing."
  fi
else
  fail "Unexpected number of stylesheets. Expected 1 but found $stylesheets."
fi

TOC_PATH='OEBPS/toc.ncx'
if [[ ! -f $TOC_PATH ]]; then
  fail "Table of contents file '$TOC_PATH' missing."
fi

TOC_NAV_LINE6=$(grep -m 1 -A 5 "<navPoint" $TOC_PATH | tail -n 1 \
  | sed -e 's/[[:blank:]]//g' -e 's/\r$//')
if [[ $TOC_NAV_LINE6 != "</navPoint>" ]]; then
  fail "Expected TOC <navPoint> tag to span exactly 6 lines."
fi

# Finally, print out
success
