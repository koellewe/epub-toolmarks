# DO NOT EXECUTE.
# This file is meant to be imported by the other scripts.

# exit script on error
set -e

# prelim checks
if [[ "$#" -ne 1 ]]; then
  echo "Please provide one argument, being the epub file to be tested."
  exit 1
fi

WORKSPACE=/tmp/toolmark-analysis
INPUT_FILE="$1"

if [[ ! -f $INPUT_FILE ]]; then
  echo "File '$INPUT_FILE' does not exist."
  exit 1
fi

INPUT_TYPE=$(file -b "$INPUT_FILE")

if [[ ! (($INPUT_TYPE == *EPUB\ document*) || ($INPUT_TYPE == *Zip\ data*)) ]]; then
  echo "File '$INPUT_FILE' is not of type EPUB or Zip."
  exit 1
fi

function cleanup(){
  if [ -d "$WORKSPACE" ]; then
    rm -rf "$WORKSPACE"
  fi
}

function fail() {
  echo "Document '$INPUT_FILE' was probably not created by $TOOL."
  echo "Reason: $1"
  cleanup
  exit 1
}

function success() {
  echo "Document '$INPUT_FILE' could have been created by $TOOL."
  cleanup
  exit 0
}

unzip -d $WORKSPACE -qq -o "$INPUT_FILE"
cd $WORKSPACE