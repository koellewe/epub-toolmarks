#!/bin/bash

export TOOL="Pressbooks (EPUB v2)"

. base.sh

OPF_PATH="book.opf"
OPFS=$(find . -name "*.opf" | wc -l)
if [[ $OPFS -eq 1 ]]; then
  if [ ! -f "$OPF_PATH" ]; then
    fail "File '$OPF_PATH' missing."
  fi
else
  fail "Bad number of OPF files. Expected 1 but found ${OPFS}."
fi

EPUB_VERSION=$(grep -m 1 "<package" "$OPF_PATH" | sed -e 's/.*version\="//' -e 's/".*//')
if [[ "$EPUB_VERSION" != "2.0" ]]; then
  fail "Wrong EPUB version. Expected 2.0 but found $EPUB_VERSION."
fi

chapter_matches=$(find OEBPS -name 'chapter-001-*.html' | wc -l)
if [[ chapter_matches -eq 0 ]]; then
  fail "First chapter file 'OEBPS/chapter-001-{name}.html' missing."
elif [[ chapter_matches -ge 2 ]]; then
  fail "Found multiple chapters with same id (001)"
fi

tab_registry=0;
IFS=''
while read line; do
  indentation_tabs=$(echo -n "${line//<*/}" | xxd -p | sed -e 's/09/t/g' -e 's/\(.\)/\1\n/g' \
    | { grep -c 't' || true; })
  if [[ $indentation_tabs -gt 2 ]]; then
    fail "Bad indentation in OPF file. Expected 0, 1, or 2 tabs."
  else
    tab_registry=$(( tab_registry | (2 ** indentation_tabs) ))
  fi
done < $OPF_PATH

if [[ $tab_registry -ne 7 ]]; then
  fail "Bad indentation in OPF file. Expected to observe all of 0, 1, and 2 tabs across the file."
fi

STYLESHEET_PATH='OEBPS/mcluhan.css'
stylesheets=$(find . -name '*.css' | wc -l)
if [[ $stylesheets -eq 1 ]]; then
  if [[ ! -f $STYLESHEET_PATH ]]; then
    fail "Stylesheet file '$STYLESHEET_PATH' missing."
  fi
else
  fail "Unexpected number of stylesheets. Expected 1 but found $stylesheets."
fi

TOC_PATH='toc.ncx'
if [[ ! -f $TOC_PATH ]]; then
  fail "Table of contents file '$TOC_PATH' missing."
fi

TOC_NAV_LINE4=$(grep -m 1 -A 3 "<navPoint" $TOC_PATH | tail -n 1 | tr -d '[:blank:]' | sed -e 's/\r$//')
if [[ $TOC_NAV_LINE4 != "</navPoint>" ]]; then
  fail "Expected TOC <navPoint> tag to span exactly 4 lines."
fi

ttfs=$(find . -name '*.ttf' | wc -l)
if [[ $ttfs -eq 0 ]]; then
  fail "Expected ttf files, but found none."
fi

COPYRIGHT_PATH="OEBPS/copyright.html"
if [[ ! -f $COPYRIGHT_PATH ]]; then
  fail "Copyright file '$COPYRIGHT_PATH' missing."
fi

PROMO_PATH="OEBPS/assets/pressbooks-promo.png"
if [[ ! -f $PROMO_PATH ]]; then
  fail "Promo file '$PROMO_PATH' missing."
fi

success
