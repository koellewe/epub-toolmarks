# EPUB Toolmarks

[![Pipeline status](https://gitlab.com/koellewe/epub-toolmarks/badges/master/pipeline.svg)](https://gitlab.com/koellewe/epub-toolmarks/-/pipelines)

Run any of the following scripts to check a particular EPUB against known toolmarks of particular EPUB creation tools:

- `epubee-toolmarks.sh`: [ePUBeeMaker](https://download.cnet.com/ePUBee-Maker/3000-20412_4-76243368.html)
- `sigil-toolmarks.sh`: [Sigil](https://sigil-ebook.com/sigil/)
- `pressbooks-ev*-toolmarks.sh`: [Pressbooks](https://github.com/pressbooks/pressbooks)
  - Pressbooks can produce EPUBs in version 2 or 3. Scripts are split accordingly. 

The first and only argument to any of these scripts should be the epub file to be tested. E.g.:

```sh
./epubee-toolmarks.sh example.epub
```

To test all creation tools in one command, use the `all-toolmarks.sh` script:

```sh
./all-toolmarks.sh example.epub
```

## Dependencies

The scripts are written for `bash`, and rely on the following commands to be available in the execution environment:

- `file`
- `unzip`
- `rm`
- `grep`
- `head`
- `tail`
- `tr`
- `sed`
- `xxd`
